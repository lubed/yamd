package conn

import (
	"database/sql/driver"
	clt "gitee.com/lubed/mysql-client"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/packet"
)

type Execer struct {
	mc  *MysqlConn
	exr *Exr
}

func NewExecer(mc *MysqlConn, exr *Exr) *Execer {
	return &Execer{
		mc:  mc,
		exr: exr,
	}
}

func (ecr *Execer) Exec(q string, args any) error {
	var prepared string
	var err error
	//var rData []byte
	query := q
	// 预处理
	if nil != args {
		vals := args.([]driver.Value)
		if prepared, err = ecr.exr.Prepared(query, vals); nil != err {
			return err
		}
		query = prepared
	}
	ecr.mc.clearResult()
	// Send command
	var pkt packet.Pkt
	var sq uint8 = 0

	if pkt, err = packet.NewCmd(mp.ComQuery, query); nil != err {
		return err
	}

	pkt.SetOption(packet.Sq, sq)

	if err = ecr.mc.ct.Send(pkt, clt.DefaultCallback); nil != err {
		//	return mc.markBadConn(err)
		//}
		//
		//
		//if err := ecr.mc.ct.WriteCommandPacketStr(mp.ComQuery, query); err != nil {
		return ecr.mc.markBadConn(err)
	}

	// Read Result
	mc := ecr.mc
	reader := mc.GetReader()
	var ay any
	var rData []byte
	var resLen int
	if ay, err = mc.GetReader()(mp.NotUntilEOF); nil != err {
		return err
	}

	rData = ay.([]byte)

	if resLen, err = mc.ct.ReadResultSetHeaderPacket(mc.GetResult(), rData, mc.GetReader()); nil != err {
		return err
	}

	if resLen > 0 {
		var sts any
		// columns
		if sts, err = reader(mp.UntilEOF); err != nil {
			return err
		}

		// rows
		if sts, err = reader(mp.UntilEOF); err != nil {
			return err
		}
		if nil != sts {
			ecr.mc.status = sts.(mp.StatusFlag)
		}
	}

	for ecr.mc.status&mp.StatusMoreResultsExists != 0 {
		if ay, err = mc.GetReader()(mp.NotUntilEOF); nil != err {
			return err
		}

		rData = ay.([]byte)

		if resLen, err = mc.ct.ReadResultSetHeaderPacket(mc.GetResult(), rData, mc.GetReader()); nil != err {
			return err
		}
		if resLen > 0 {
			var sts any
			// columns
			if sts, err = reader(mp.UntilEOF); err != nil {
				return err
			}
			// rows
			if sts, err = reader(mp.UntilEOF); err != nil {
				return err
			}
			if nil != sts {
				ecr.mc.status = sts.(mp.StatusFlag)
			}
		}
	}

	return nil
}
