package conn

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"errors"
	"gitee.com/lubed/client"
	clt "gitee.com/lubed/mysql-client"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/dsn"
	"gitee.com/lubed/mysql-protocol/helper"
	"gitee.com/lubed/mysql-protocol/packet"
	"gitee.com/lubed/mysql-protocol/parser"
)

type MysqlConn struct {
	ct      *clt.MyClient //客户端
	reader  client.Reader //包读取
	closer  client.Closer //连接关闭
	execer  MyExecer      //命令执行
	isDebug bool
	result  mp.MysqlResult
	cfg     *dsn.Config       //配置
	flags   mp.CapabilityFlag //客户端能力
	status  mp.StatusFlag
	connId  uint32
}

func NewMyConn(cfg *dsn.Config, isDebug bool) *MysqlConn {
	mc := &MysqlConn{
		cfg:     cfg,
		isDebug: isDebug,
	}

	return mc
}

// Begin 实现 driver.Conn 接口中的Begin方法,开启事务
func (mc *MysqlConn) Begin() (driver.Tx, error) {
	return mc.begin(false)
}

// Close 实现 driver.Conn 接口中的Close方法
func (mc *MysqlConn) Close() (err error) {
	if err = mc.ct.Quit(); nil != err {
		return err
	}
	mc.Cleanup()
	return
}

func (mc *MysqlConn) Prepare(query string) (driver.Stmt, error) {
	var err error
	var ay any
	if err = mc.ct.SendStmtPrepare(query); nil != err {
		return nil, err
	}
	stmt := &mysqlStmt{
		mc: mc,
	}

	if ay, err = mc.ct.Receive(mp.NotUntilEOF, mc.closer); nil != err {
		return nil, err
	}

	data := ay.([]byte)
	// Read Result
	columnCount, err := stmt.readPrepareResultPacket(data, mc.flags, mc.cfg.RejectReadOnly)

	if err == nil {
		if stmt.paramCount > 0 {
			if _, err = mc.ct.Receive(mp.UntilEOF, mc.closer); err != nil {
				return nil, err
			}
		}

		if columnCount > 0 {
			_, err = mc.ct.Receive(mp.UntilEOF, mc.closer)
		}
	}

	return stmt, err
}

func (mc *MysqlConn) Exec(query string, args []driver.Value) (driver.Result, error) {
	if mc.ct.Closed() {
		mc.GetConfig().Logger.Print(ErrInvalidConn)
		return nil, driver.ErrBadConn
	}
	if err := mc.exec(query, args); nil != err {
		return nil, mc.markBadConn(err)
	}

	copied := mc.result
	return &copied, nil
}

func (mc *MysqlConn) Query(query string, args []driver.Value) (driver.Rows, error) {
	return mc.query(query, args)
}

// Ping implements driver.Pinger interface
func (mc *MysqlConn) Ping(ctx context.Context) (err error) {
	var pkt packet.Pkt
	var sq uint8 = 0

	if mc.ct.Closed() {
		mc.cfg.Logger.Print(ErrInvalidConn)
		return driver.ErrBadConn
	}

	if err = mc.ct.WatchCancel(ctx, mc.closer); err != nil {
		return
	}

	defer mc.ct.Finish()
	mc.clearResult()

	if pkt, err = packet.NewCmd(mp.ComPing, nil); nil != err {
		return err
	}

	pkt.SetOption(packet.Sq, sq)

	if err = mc.ct.Send(pkt, clt.DefaultCallback); nil != err {
		return mc.markBadConn(err)
	}

	return mc.readResultOK()
}

// BeginTx implements driver.ConnBeginTx interface
func (mc *MysqlConn) BeginTx(ctx context.Context, opts driver.TxOptions) (driver.Tx, error) {
	var err error
	var level string

	if mc.ct.Closed() {
		return nil, driver.ErrBadConn
	}

	if err = mc.ct.WatchCancel(ctx, mc.closer); nil != err {
		return nil, err
	}

	defer mc.ct.Finish()

	if sql.IsolationLevel(opts.Isolation) != sql.LevelDefault {
		if level, err = helper.MapIsolationLevel(opts.Isolation); nil != err {
			return nil, err
		}
		if err = mc.exec("SET TRANSACTION ISOLATION LEVEL "+level, nil); nil != err {
			return nil, err
		}
	}

	return mc.begin(opts.ReadOnly)
}

// QueryContext 实现driver.QueryerContext接口
func (mc *MysqlConn) QueryContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Rows, error) {
	var dArgs []driver.Value
	var err error
	var rows *TextRows

	if dArgs, err = helper.NamedValueToValue(args); nil != err {
		return nil, err
	}

	if err = mc.ct.WatchCancel(ctx, mc.closer); nil != err {
		return nil, err
	}

	if rows, err = mc.query(query, dArgs); nil != err {
		mc.ct.Finish()
		return nil, err
	}

	rows.finish = mc.ct.Finish

	return rows, err
}

func (mc *MysqlConn) ExecContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Result, error) {
	dargs, err := helper.NamedValueToValue(args)

	if err != nil {
		return nil, err
	}
	if err = mc.ct.WatchCancel(ctx, mc.closer); err != nil {
		return nil, err
	}

	defer mc.ct.Finish()

	return mc.Exec(query, dargs)
}

func (mc *MysqlConn) PrepareContext(ctx context.Context, query string) (driver.Stmt, error) {
	if err := mc.ct.WatchCancel(ctx, mc.closer); err != nil {
		return nil, err
	}

	stmt, err := mc.Prepare(query)
	mc.ct.Finish()

	if err != nil {
		return nil, err
	}

	select {
	default:
	case <-ctx.Done():
		err = stmt.Close()
		return nil, ctx.Err()
	}

	return stmt, nil
}

func (mc *MysqlConn) CheckNamedValue(nv *driver.NamedValue) (err error) {
	nv.Value, err = helper.Converter{}.ConvertValue(nv.Value)
	return
}

// ResetSession implements driver.SessionResetter.
// (From Go 1.10)
func (mc *MysqlConn) ResetSession(ctx context.Context) error {
	return mc.ct.Reset()
	//if mc.closed.Load() {
	//	return driver.ErrBadConn
	//}
	//
	//// Perform a stale connection check. We only perform this check for
	//// the first query on a connection that has been checked out of the
	//// connection pool: a fresh connection from the pool is more likely
	//// to be stale, and it has not performed any previous writes that
	//// could cause data corruption, so it's safe to return ErrBadConn
	//// if the check fails.
	//if mc.cfg.CheckConnLiveness {
	//	conn := mc.NetConn
	//	if mc.rawConn != nil {
	//		conn = mc.rawConn
	//	}
	//	var err error
	//	if mc.cfg.ReadTimeout != 0 {
	//		err = conn.SetReadDeadline(time.Now().Add(mc.cfg.ReadTimeout))
	//	}
	//	if err == nil {
	//		err = connCheck(conn)
	//	}
	//	if err != nil {
	//		mc.cfg.Logger.Print("closing bad idle connection: ", err)
	//		return driver.ErrBadConn
	//	}
	//}
	//
	//return nil
}

// IsValid implements driver.Validator interface
// (From Go 1.15)
func (mc *MysqlConn) IsValid() bool {
	return !mc.ct.Closed()
}

// Canceling 取消
func (mc *MysqlConn) Canceling(ctx context.Context) error {
	var fn client.Closer
	fn = func() error { mc.clearResult(); return nil }
	if err := mc.ct.WatchCancel(ctx, fn); nil != err {
		return err
	}
	return nil
}

// Closes the network connection and unsets internal variables. Do not call this
// function after successfully authentication, call Close instead. This function
// is called before auth or on auth failure because MySQL will have already
// closed the network connection.
func (mc *MysqlConn) Cleanup() {
	mc.ct.Cleanup()
	mc.clearResult()
}

// GetClient 获取MySQL客户端
func (mc *MysqlConn) GetClient() *clt.MyClient {
	return mc.ct
}

func (mc *MysqlConn) GetCloser() client.Closer {
	return mc.closer
}

// GetConfig 获取DSN配置
func (mc *MysqlConn) GetConfig() *dsn.Config {
	return mc.cfg
}

func (mc *MysqlConn) GetExecer() MyExecer {
	return mc.execer
}

func (mc *MysqlConn) GetReader() client.Reader {
	return mc.reader
}

func (mc *MysqlConn) GetResult() *mp.MysqlResult {
	return &mc.result
}

func (mc *MysqlConn) GetStatus() mp.StatusFlag {
	return mc.status
}

func (mc *MysqlConn) Init(ctx context.Context, ct *clt.MyClient) error {
	var err error

	mc.ct = ct

	if err = mc.ct.CreateConnection(ctx); nil != err {
		return err
	}

	// Call startWatcher for context support (From Go 1.8)
	mc.ct.StartWatcher()

	if err := mc.ct.WatchCancel(ctx, mc.closer); err != nil {
		mc.Cleanup()
		return err
	}

	defer mc.ct.Finish()
	mc.closer = func() error { return mc.Close() }
	mc.reader = func(untilEOF bool) (any, error) {
		return mc.ct.Receive(untilEOF, mc.closer)
	}
	mc.execer = func(query string, args any) error {
		exr := NewExr(mc.cfg, mc.ct.GetBuf(), mc.status, mc.ct.GetMxAwPt())
		ecr := NewExecer(mc, exr)
		return ecr.Exec(query, args)
	}

	return nil
}

// SetConnId 设置连接id
func (mc *MysqlConn) SetConnId(id uint32) {
	mc.connId = id
}

func (mc *MysqlConn) Status(sts mp.StatusFlag) {
	mc.status = sts
}

func (mc *MysqlConn) begin(readOnly bool) (driver.Tx, error) {
	if mc.ct.Closed() {
		mc.cfg.Logger.Print(ErrInvalidConn)
		return nil, driver.ErrBadConn
	}
	var q string
	if readOnly {
		q = "START TRANSACTION READ ONLY"
	} else {
		q = "START TRANSACTION"
	}
	err := mc.exec(q, nil)
	if err == nil {
		return &mysqlTx{mc}, err
	}
	return nil, mc.markBadConn(err)
}

// Internal function to execute commands
func (mc *MysqlConn) exec(query string, args any) error {
	if err := mc.execer(query, args); nil != err {
		return mc.markBadConn(err)
	}
	return nil
}

func (mc *MysqlConn) markBadConn(err error) error {
	if mc == nil {
		return err
	}
	if !errors.Is(err, errBadConnNoWrite) {
		return err
	}
	return driver.ErrBadConn
}
func (mc *MysqlConn) query(query string, args []driver.Value) (*TextRows, error) {
	mc.clearResult()
	exr := NewExr(mc.GetConfig(), mc.ct.GetBuf(), mc.status, mc.ct.GetMxAwPt())
	mqy := NewQueryer(mc, exr)
	return mqy.Query(query, args)
}

// Returns error if Packet is not a 'Result OK'-Packet
func (mc *MysqlConn) readResultOK() error {
	var sts *mp.StatusFlag
	ay, err := mc.ct.Receive(mp.NotUntilEOF, mc.closer)
	data := ay.([]byte)

	if err != nil {
		return err
	}
	mpr := parser.NewMyParser()
	if data[0] == mp.OK {
		if sts, err = mpr.ParseOkPacket(&mc.result, data); nil != err {
			mc.status = *sts
		}
		return nil
	}

	return mpr.ParseErrorPacket(data, mc.cfg.RejectReadOnly)
}

// clearResult clears the connection's stored affectedRows and insertIds
// fields.
//
// It returns a handler that can process OK responses.
func (mc *MysqlConn) clearResult() {
	mc.result = mp.MysqlResult{}
}
