package attr

import (
	"gitee.com/lubed/mysql-protocol/bdt"
	"gitee.com/lubed/mysql-protocol/dsn"
	"net"
	"os"
	"runtime"
	"strconv"
	"strings"
)

// Connection attributes
// See https://dev.mysql.Com/doc/refman/8.0/en/performance-schema-Connection-attribute-tables.html#performance-schema-Connection-attributes-available

const (
	connAttrClientName      = "_Client_name"
	connAttrClientNameValue = "YAMD"
	connAttrOS              = "_os"
	connAttrOSValue         = runtime.GOOS
	connAttrPlatform        = "_platform"
	connAttrPlatformValue   = runtime.GOARCH
	connAttrPid             = "_pid"
	connAttrServerHost      = "_server_host"
)

func EncodeConnectionAttributes(cfg *dsn.Config) string {
	connAttrsBuf := make([]byte, 0)
	lei := bdt.LenEncodedInt()

	// default connection attributes
	connAttrsBuf = lei.AppendStr(connAttrClientName, connAttrsBuf)
	connAttrsBuf = lei.AppendStr(connAttrClientNameValue, connAttrsBuf)
	connAttrsBuf = lei.AppendStr(connAttrOS, connAttrsBuf)
	connAttrsBuf = lei.AppendStr(connAttrOSValue, connAttrsBuf)
	connAttrsBuf = lei.AppendStr(connAttrPlatform, connAttrsBuf)
	connAttrsBuf = lei.AppendStr(connAttrPlatformValue, connAttrsBuf)
	connAttrsBuf = lei.AppendStr(connAttrPid, connAttrsBuf)
	connAttrsBuf = lei.AppendStr(strconv.Itoa(os.Getpid()), connAttrsBuf)
	serverHost, _, _ := net.SplitHostPort(cfg.Addr)
	if serverHost != "" {
		connAttrsBuf = lei.AppendStr(connAttrServerHost, connAttrsBuf)
		connAttrsBuf = lei.AppendStr(serverHost, connAttrsBuf)
	}

	// user-defined connection attributes
	for _, connAttr := range strings.Split(cfg.ConnectionAttributes, ",") {
		k, v, found := strings.Cut(connAttr, ":")
		if !found {
			continue
		}
		connAttrsBuf = lei.AppendStr(k, connAttrsBuf)
		connAttrsBuf = lei.AppendStr(v, connAttrsBuf)
	}

	return string(connAttrsBuf)
}
