package conn

import (
	mp "gitee.com/lubed/mysql-protocol"
	"strings"
)

type DsnParamBuilder struct{}

func NewDsnParamBuilder() *DsnParamBuilder {
	return &DsnParamBuilder{}
}

// Build parameters set in DSN after the connection is established
func (dpb *DsnParamBuilder) Build(params map[string]string, collation string) string {
	var cmdSet strings.Builder
	var q string

	for param, val := range params {
		if "charset" == param {
			charsets := strings.Split(val, ",")
			if q = dpb.buildCharsets(charsets, collation); mp.EMPTY != q {
				return q
			}
		}
		if 0 == cmdSet.Len() {
			cmdSet.Grow(4 + len(param) + 3 + len(val) + 30*(len(params)-1))
			cmdSet.WriteString("SET ")
		} else {
			cmdSet.WriteString(", ")
		}
		cmdSet.WriteString(param)
		cmdSet.WriteString(" = ")
		cmdSet.WriteString(val)
	}

	if cmdSet.Len() > 0 {
		return cmdSet.String()
	}

	return mp.EMPTY
}

func (dpb *DsnParamBuilder) buildCharsets(charsets []string, collation string) string {
	var q string
	for _, cs := range charsets {
		q = "SET NAMES " + cs

		if mp.EMPTY != collation {
			q += " COLLATE " + collation
		}
		return q
	}
	return mp.EMPTY
}
