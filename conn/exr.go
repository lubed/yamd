package conn

import (
	"database/sql/driver"
	"gitee.com/lubed/buf/dblbuf"
	"gitee.com/lubed/logger"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/dsn"
)

type Exr struct {
	cfg            *dsn.Config
	buf            dblbuf.DblBf
	sts            mp.StatusFlag
	maxAllowPacket int
	rst            mp.MyResultSet
	cf             mp.CapabilityFlag
}

func NewExr(cfg *dsn.Config, buf dblbuf.DblBf, sts mp.StatusFlag, maxAllowPkt int) *Exr {
	return &Exr{cfg: cfg, buf: buf, sts: sts, maxAllowPacket: maxAllowPkt}
}

func (er *Exr) Prepared(query string, vals []driver.Value) (string, error) {
	if len(vals) != 0 {
		cfg := er.cfg
		if !cfg.InterpolateParams {
			return query, driver.ErrSkip
		}

		bf := er.buf
		data, err := bf.GetAll()
		if nil != err {
			logger.DefaultLogger.Print(err)
			return query, ErrInvalidConn
		}

		po := NewParamCf(er.sts, cfg.Loc, cfg.TimeTruncate, er.maxAllowPacket)
		return InterpolateParams(query, vals, data, po)
	}
	return query, nil
}
