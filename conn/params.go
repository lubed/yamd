package conn

import (
	"database/sql/driver"
	"encoding/json"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/helper"
	"strconv"
	"strings"
	"time"
)

type ParamOptions struct {
	Sts              mp.StatusFlag
	Loc              *time.Location
	TimeTruncate     time.Duration
	MaxAllowedPacket int
}

func NewParamCf(sts mp.StatusFlag, loc *time.Location, timeTruncate time.Duration, mxAwPt int) *ParamOptions {
	return &ParamOptions{
		Sts:              sts,
		Loc:              loc,
		TimeTruncate:     timeTruncate,
		MaxAllowedPacket: mxAwPt,
	}
}

func InterpolateParams(query string, args []driver.Value, bf []byte, po *ParamOptions) (string, error) {
	var err error
	// Number of ? should be same to len(args)
	if strings.Count(query, "?") != len(args) {
		return "", driver.ErrSkip
	}

	bf = bf[:0]
	argPos := 0

	for i := 0; i < len(query); i++ {
		q := strings.IndexByte(query[i:], '?')
		if q == -1 {
			bf = append(bf, query[i:]...)
			break
		}
		bf = append(bf, query[i:i+q]...)
		i += q

		arg := args[argPos]
		argPos++

		if nil == arg {
			bf = append(bf, "NULL"...)
			continue
		}

		switch v := arg.(type) {
		case int64:
			bf = strconv.AppendInt(bf, v, 10)
		case uint64:
			// Handle uint64 explicitly because our custom ConvertValue emits unsigned values
			bf = strconv.AppendUint(bf, v, 10)
		case float64:
			bf = strconv.AppendFloat(bf, v, 'g', -1, 64)
		case bool:
			if v {
				bf = append(bf, '1')
			} else {
				bf = append(bf, '0')
			}
		case time.Time:
			if v.IsZero() {
				bf = append(bf, "'0000-00-00'"...)
			} else {
				bf = append(bf, '\'')
				bf, err = helper.AppendDateTime(bf, v.In(po.Loc), po.TimeTruncate)

				if err != nil {
					return "", err
				}

				bf = append(bf, '\'')
			}
		case json.RawMessage:
			bf = append(bf, '\'')
			if po.Sts&mp.StatusNoBackslashEscapes == 0 {
				bf = helper.EscapeBytesBackslash(bf, v)
			} else {
				bf = helper.EscapeBytesQuotes(bf, v)
			}
			bf = append(bf, '\'')
		case []byte:
			if v == nil {
				bf = append(bf, "NULL"...)
			} else {
				bf = append(bf, "_binary'"...)
				if po.Sts&mp.StatusNoBackslashEscapes == 0 {
					bf = helper.EscapeBytesBackslash(bf, v)
				} else {
					bf = helper.EscapeBytesQuotes(bf, v)
				}
				bf = append(bf, '\'')
			}
		case string:
			bf = append(bf, '\'')
			if po.Sts&mp.StatusNoBackslashEscapes == 0 {
				bf = helper.EscapeStringBackslash(bf, v)
			} else {
				bf = helper.EscapeStringQuotes(bf, v)
			}
			bf = append(bf, '\'')
		default:
			return "", driver.ErrSkip
		}

		if len(bf)+4 > po.MaxAllowedPacket {
			return "", driver.ErrSkip
		}
	}
	if argPos != len(args) {
		return "", driver.ErrSkip
	}
	return string(bf), nil
}
