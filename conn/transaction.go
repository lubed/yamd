package conn

type mysqlTx struct {
	mc *MysqlConn
}

func (tx *mysqlTx) Commit() (err error) {
	if tx.mc == nil || tx.mc.ct.Closed() {
		return ErrInvalidConn
	}
	err = tx.mc.exec("COMMIT", nil)
	tx.mc = nil
	return
}

func (tx *mysqlTx) Rollback() (err error) {
	if tx.mc == nil || tx.mc.ct.Closed() {
		return ErrInvalidConn
	}
	err = tx.mc.exec("ROLLBACK", nil)
	tx.mc = nil
	return
}
