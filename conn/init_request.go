package conn

import (
	"database/sql/driver"
	"gitee.com/lubed/client"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/helper"
	"gitee.com/lubed/mysql-protocol/packet"
	"gitee.com/lubed/yamd/excep"
)

type MyExecer func(query string, args any) error

// MyInitRequest MySQL客户端连接到服务器 发送 初始化 相关操作请求
type MyInitRequest struct {
	mc *MysqlConn
}

// NewMyInitRequest 新建初始化请求
func NewMyInitRequest(mc *MysqlConn) *MyInitRequest {
	return &MyInitRequest{mc: mc}
}

func (mir *MyInitRequest) getMaxAllowPacket() (int, error) {
	var maxAllowPkt []byte
	var err error
	cfg := mir.mc.GetConfig()
	if cfg.MaxAllowedPacket > 0 {
		return cfg.MaxAllowedPacket, nil
	}
	//从MySQL服务器获取max_allowed_packet变量
	if maxAllowPkt, err = mir.getServerVariable("max_allowed_packet"); nil != err {
		mir.mc.Close()
		return 0, err
	}
	return helper.StringToInt(maxAllowPkt) - 1, nil

}

// getServerVariable  获取指定名称的MySQL服务器变量
func (mir *MyInitRequest) getServerVariable(name string) ([]byte, error) {
	//var rData []byte
	var err error
	var trs *TextRows
	var ay any
	var resLen int

	mc := mir.mc
	reader := mc.GetReader()

	var pkt packet.Pkt
	var sq uint8 = 0
	if pkt, err = packet.NewCmd(mp.ComQuery, "SELECT @@"+name); nil != err {
		return nil, err
	}

	pkt.SetOption(packet.Sq, sq)
	snr := mc.ct.GetSender()
	snr.Init(pkt)
	callback := func(sr client.SendResult) error {
		if excep.None == sr.Code {
			var ay any
			if ay, err = pkt.GetOption(packet.Sq); nil == err {
				mc.ct.Sequence = ay.(uint8) + 1
			}
		}
		return nil
	}
	if err = snr.Send(callback); nil != err {
		return nil, err
	}

	var rData []byte
	if ay, err = reader(mp.NotUntilEOF); nil != err {
		return nil, err
	}

	rData = ay.([]byte)

	if resLen, err = mir.mc.ct.ReadResultSetHeaderPacket(mc.GetResult(), rData, mc.GetReader()); nil != err {
		return nil, err
	}

	trs = new(TextRows)
	trs.mc = mir.mc
	trs.rs.Columns([]mp.MysqlField{{FieldType: mp.FieldTypeVarChar}})

	if resLen > 0 {
		// read columns
		if ay, err = reader(mp.UntilEOF); err != nil {
			return nil, err
		}
		if nil != ay {
			mir.mc.Status(ay.(mp.StatusFlag))
		}
	}

	dest := make([]driver.Value, resLen)
	if err = trs.readRow(dest); nil != err {
		return nil, err

	}

	if ay, err = reader(mp.UntilEOF); nil != ay {
		mir.mc.Status(ay.(mp.StatusFlag))
	}
	return dest[0].([]byte), err
}

func (mir *MyInitRequest) DoInitialize() error {
	var maxAllowPacket int
	var err error

	if maxAllowPacket, err = mir.getMaxAllowPacket(); nil != err {
		return err
	}

	mir.mc.GetClient().SetSize(maxAllowPacket)
	if err = mir.sendCmdByParams(); nil != err {
		return err
	}
	return nil
}

// SendCmdByParams 根据参数向MySQL服务器发送命令
func (mir *MyInitRequest) sendCmdByParams() (err error) {
	var cmd string

	cfg := mir.mc.GetConfig()
	execer := mir.mc.GetExecer()
	dpb := NewDsnParamBuilder()
	cmd = dpb.Build(cfg.Params, cfg.Collation)

	if mp.EMPTY != cmd {
		if err = execer(cmd, nil); nil != err {
			return err
		}
	}

	return nil
}
