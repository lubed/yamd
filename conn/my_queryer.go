package conn

import (
	"database/sql/driver"
	"fmt"
	"gitee.com/lubed/client"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/packet"
	"gitee.com/lubed/yamd/excep"
	"io"
)

type MyQueryer struct {
	mc  *MysqlConn
	exr *Exr
}

func NewQueryer(mc *MysqlConn, exr *Exr) *MyQueryer {
	return &MyQueryer{mc: mc, exr: exr}
}

func (mqr *MyQueryer) Query(q string, args []driver.Value) (*TextRows, error) {
	var rData []byte
	var err error
	query := q

	if mqr.mc.ct.Closed() {
		mqr.mc.GetConfig().Logger.Print(ErrInvalidConn)
		return nil, driver.ErrBadConn
	}

	// 预处理
	if nil != args {
		if query, err = mqr.exr.Prepared(q, args); nil != err {
			return nil, err
		}
	}

	if mqr.mc.ct.IsDebug() {
		fmt.Println("[YAMD]", "query:", query, args, "...ok")
	}

	var pkt packet.Pkt
	var sq uint8 = 0

	if pkt, err = packet.NewCmd(mp.ComQuery, query); nil != err {
		return nil, err
	}

	pkt.SetOption(packet.Sq, sq)
	snr := mqr.mc.ct.GetSender()
	snr.Init(pkt)
	callback := func(sr client.SendResult) error {
		if excep.None == sr.Code {
			var ay any
			if ay, err = pkt.GetOption(packet.Sq); nil == err {
				mqr.mc.ct.Sequence = ay.(uint8) + 1
			}
		}
		return nil
	}
	if err = snr.Send(callback); nil != err {
		return nil, mqr.mc.markBadConn(err)
	}

	// Read Result
	var resLen int
	var ay any
	if ay, err = mqr.mc.reader(mp.NotUntilEOF); nil != err {
		return nil, err
	}
	rData = ay.([]byte)
	resLen, err = mqr.mc.ct.ReadResultSetHeaderPacket(mqr.mc.GetResult(), rData, mqr.mc.reader)

	if err == nil {
		trs := new(TextRows)
		trs.mc = mqr.mc

		if resLen == 0 {
			trs.rs.Done()
			err = trs.NextResultSet()
			switch err {
			case nil, io.EOF:
				return trs, nil
			default:
				return nil, err
			}
		}

		// Columns
		//var columns []mp.MysqlField
		err = trs.readColumns(resLen)

		return trs, err
	}
	return nil, mqr.mc.markBadConn(err)
}
