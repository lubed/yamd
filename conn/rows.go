package conn

import (
	"fmt"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/parser"
	"io"
)

type mysqlRows struct {
	mc     *MysqlConn
	rs     mp.MyResultSet
	finish func()
}

func (rws *mysqlRows) Columns() []string {
	if rws.rs.CmnNames != nil {
		return rws.rs.CmnNames
	}

	columns := make([]string, rws.rs.Count())

	for i := range columns {
		field, err := rws.rs.GetColumnByIndex(i)
		if nil != err {
			continue
		}
		columns[i] = field.Name
		if rws.mc.GetConfig().ColumnsWithAlias {
			if tableName := field.TableName; len(tableName) > 0 {
				columns[i] = tableName + "." + field.Name
			}
		}
	}

	rws.rs.CmnNames = columns
	return columns
}

func (rws *mysqlRows) Close() (err error) {
	if f := rws.finish; f != nil {
		f()
		rws.finish = nil
	}

	mc := rws.mc
	if mc == nil {
		return nil
	}
	if err := mc.ct.Error(); err != nil {
		return err
	}

	// flip the buffer for this connection if we need to drain it.
	// note that for a successful query (i.e. one where rws.next()
	// has been called until it returns false), `rws.mc` will be nil
	// by the time the user calls `(*Rows).Close`, so we won't reach this
	// see: https://github.com/golang/go/commit/651ddbdb5056ded455f47f9c494c67b389622a47
	bf := mc.ct.GetBuf()
	bf.Flip()

	// Remove unread packets from stream
	if !rws.rs.IsDone() {
		_, err = mc.ct.Receive(mp.UntilEOF, mc.closer)
	}
	var sts any
	var resLen int
	var ay any
	var rData []byte
	if err == nil {
		mc.clearResult()

		for mc.status&mp.StatusMoreResultsExists != 0 {
			if ay, err = mc.GetReader()(mp.NotUntilEOF); nil != err {
				return err
			}

			rData = ay.([]byte)

			if resLen, err = mc.ct.ReadResultSetHeaderPacket(mc.GetResult(), rData, mc.GetReader()); nil != err {
				return err
			}
			if resLen > 0 {
				// columns
				if sts, err = mc.ct.Receive(mp.UntilEOF, mc.closer); err != nil {
					return err
				}
				mc.status = sts.(mp.StatusFlag)
				// rows
				if sts, err = mc.ct.Receive(mp.UntilEOF, mc.closer); err != nil {
					return err
				}
				mc.status = sts.(mp.StatusFlag)
			}
		}
	}

	rws.mc = nil
	return err
}

func (rws *mysqlRows) HasNextResultSet() (b bool) {
	if rws.mc == nil {
		return false
	}
	return rws.mc.status&mp.StatusMoreResultsExists != 0
}

func (rws *mysqlRows) nextResultSet() (int, error) {
	if rws.mc == nil {
		return 0, io.EOF
	}
	if err := rws.mc.ct.Error(); err != nil {
		return 0, err
	}

	// Remove unread packets from stream
	if !rws.rs.IsDone() {
		if _, err := rws.mc.ct.Receive(mp.UntilEOF, rws.mc.GetCloser()); err != nil {
			return 0, err
		}
		rws.rs.Done()
	}

	if !rws.HasNextResultSet() {
		rws.mc = nil
		return 0, io.EOF
	}
	rws.rs = mp.MyResultSet{}

	// rws.mc.affectedRows and rws.mc.insertIds accumulate on each call to
	// nextResultSet.
	var ay any
	var rData []byte
	var err error
	var resLen int
	if ay, err = rws.mc.GetReader()(mp.NotUntilEOF); nil != err {
		return 0, err
	}

	rData = ay.([]byte)

	if resLen, err = rws.mc.ct.ReadResultSetHeaderPacket(rws.mc.GetResult(), rData, rws.mc.GetReader()); nil != err {
		// Clean up about multi-results flag
		rws.rs.Done()
		rws.mc.status = rws.mc.status & (^mp.StatusMoreResultsExists)
	}
	return resLen, err
}

func (rws *mysqlRows) nextNotEmptyResultSet() (int, error) {
	for {
		resLen, err := rws.nextResultSet()
		if err != nil {
			return 0, err
		}

		if resLen > 0 {
			return resLen, nil
		}

		rws.rs.Done()
	}
}

// ReadColumns Read Packets as Field Packets until EOF-Packet or an Error appears
// http://dev.mysql.com/doc/internals/en/com-query-response.html#packet-Protocol::ColumnDefinition41
func (rws *mysqlRows) readColumns(count int) error {
	var ay any
	var err error
	var cln *mp.MysqlField
	var data []byte

	columns := make([]mp.MysqlField, count)
	rwP := parser.NewRowParser()
	reader := rws.mc.GetReader()
	cfg := rws.mc.GetConfig()

	for i := 0; ; i++ {
		if ay, err = reader(mp.NotUntilEOF); nil != err {
			return err
		}
		data = ay.([]byte)

		// EOF Packet
		if data[0] == mp.EOF && (len(data) == 5 || len(data) == 1) {
			if i == count {
				rws.rs.Columns(columns)
				return nil
			}
			return fmt.Errorf("column count mismatch n:%d len:%d", count, len(columns))
		}

		cln, err = rwP.Parse(data, cfg.ColumnsWithAlias)
		columns[i] = *cln
	}
}
