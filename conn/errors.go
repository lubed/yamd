package conn

import (
	"errors"
)

// Various errors the driver might return. Can change between driver versions.
var (
	ErrInvalidConn = errors.New("invalid connection")
	ErrMalformPkt  = errors.New("malformed packet")
	ErrNoTLS       = errors.New("TLS requested but server does not support TLS")
	ErrOldProtocol = errors.New("MySQL server does not support required protocol 41+")
	ErrPktSync     = errors.New("commands out of sync. You can't run this command now")
	ErrPktSyncMul  = errors.New("conn:commands out of sync. Did you run multiple statements at once?")
	ErrPktTooLarge = errors.New("packet for query is too large. Try adjusting the `Config.maxAllowedPacket`")
	ErrBusyBuffer  = errors.New("busy buffer")

	// errBadConnNoWrite is used for connection errors where nothing was sent to the database yet.
	// If this happens first in a function starting a database interaction, it should be replaced by driver.ErrBadConn
	// to trigger a resend.
	// See https://gitee.com/lubed/mysql/pull/302
	errBadConnNoWrite = errors.New("bad connection")
)

//// MySQLError is an error type which represents a single MySQL error
//type MySQLError struct {
//	Number   uint16
//	SQLState [5]byte
//	Message  string
//}
//
//func (me *MySQLError) Error() string {
//	if me.SQLState != [5]byte{} {
//		return fmt.Sprintf("Error %d (%s): %s", me.Number, me.SQLState, me.Message)
//	}
//
//	return fmt.Sprintf("Error %d: %s", me.Number, me.Message)
//}
//
//func (me *MySQLError) Is(err error) bool {
//	if merr, ok := err.(*MySQLError); ok {
//		return merr.Number == me.Number
//	}
//	return false
//}
