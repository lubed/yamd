package conn

import (
	"database/sql/driver"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/bdt"
	"gitee.com/lubed/mysql-protocol/helper"
	"gitee.com/lubed/mysql-protocol/parser"
	"io"
	"strconv"
)

type TextRows struct {
	mysqlRows
}

func (trs *TextRows) NextResultSet() (err error) {
	resLen, err := trs.nextNotEmptyResultSet()
	if err != nil {
		return err
	}

	err = trs.readColumns(resLen)
	return err
}

func (trs *TextRows) Next(dest []driver.Value) error {
	if mc := trs.mc; mc != nil {
		if err := mc.ct.Error(); err != nil {
			return err
		}

		// Fetch next row from stream
		return trs.readRow(dest)
	}
	return io.EOF
}

// Read Packets as Field Packets until EOF-Packet or an Error appears
// http://dev.mysql.com/doc/internals/en/com-query-response.html#packet-ProtocolText::ResultsetRow
func (trs *TextRows) readRow(dest []driver.Value) error {
	var ay any
	var err error
	mc := trs.mc

	if trs.rs.IsDone() {
		return io.EOF
	}

	ay, err = mc.ct.Receive(mp.NotUntilEOF, mc.closer)
	if err != nil {
		return err
	}
	data := ay.([]byte)

	// EOF Packet
	if data[0] == mp.EOF && len(data) == 5 {
		// server_status [2 bytes]
		trs.mc.status = mp.ReadStatus(data[3:])
		trs.rs.Done()
		if !trs.HasNextResultSet() {
			trs.mc = nil
		}
		return io.EOF
	}
	if data[0] == mp.ERR {
		trs.mc = nil
		mpr := parser.NewMyParser()
		return mpr.ParseErrorPacket(data, mc.GetConfig().RejectReadOnly)
	}

	// RowSet Packet
	var (
		n      int
		isNull bool
		pos    int = 0
	)

	lei := bdt.LenEncodedInt()

	for i := range dest {
		// Read bytes and convert to string
		var buf []byte
		buf, isNull, n, err = lei.DecodeStr(data[pos:])
		pos += n

		if err != nil {
			return err
		}

		if isNull {
			dest[i] = nil
			continue
		}
		field, err := trs.rs.GetColumnByIndex(i)
		switch field.FieldType {
		case mp.FieldTypeTimestamp,
			mp.FieldTypeDateTime,
			mp.FieldTypeDate,
			mp.FieldTypeNewDate:
			if mc.GetConfig().ParseTime {
				dest[i], err = helper.ParseDateTime(buf, mc.GetConfig().Loc)
			} else {
				dest[i] = buf
			}

		case mp.FieldTypeTiny, mp.FieldTypeShort, mp.FieldTypeInt24, mp.FieldTypeYear, mp.FieldTypeLong:
			dest[i], err = strconv.ParseInt(string(buf), 10, 64)

		case mp.FieldTypeLongLong:
			if field.Flags&mp.FlagUnsigned != 0 {
				dest[i], err = strconv.ParseUint(string(buf), 10, 64)
			} else {
				dest[i], err = strconv.ParseInt(string(buf), 10, 64)
			}

		case mp.FieldTypeFloat:
			var d float64
			d, err = strconv.ParseFloat(string(buf), 32)
			dest[i] = float32(d)

		case mp.FieldTypeDouble:
			dest[i], err = strconv.ParseFloat(string(buf), 64)

		default:
			dest[i] = buf
		}
		if err != nil {
			return err
		}
	}

	return nil
}
