package conn

import (
	"database/sql/driver"
	"fmt"
	"gitee.com/lubed/mysql-protocol/helper"
	"gitee.com/lubed/mysql-protocol/parser"
	"io"

	mp "gitee.com/lubed/mysql-protocol"
)

type mysqlStmt struct {
	mc         *MysqlConn
	id         uint32
	paramCount int
}

// Close 实现driver.Stmt接口
func (stmt *mysqlStmt) Close() error {
	var err error

	if stmt.mc == nil || stmt.mc.ct.Closed() {
		// driver.Stmt.Close can be called more than once
		return driver.ErrBadConn
	}

	err = stmt.mc.GetClient().SendStmtClose(stmt.id)
	stmt.mc = nil
	return err
}

// NumInput 继承driver.Stmt接口
func (stmt *mysqlStmt) NumInput() int {
	return stmt.paramCount
}

// ColumnConverter
// Deprecated: 废弃,实现 driver.NamedValueChecker 接口代替
func (stmt *mysqlStmt) ColumnConverter(idx int) driver.ValueConverter {
	return helper.NewConverter()
}

// CheckNamedValue 实现 driver.NamedValueChecker 接口
func (stmt *mysqlStmt) CheckNamedValue(nv *driver.NamedValue) (err error) {
	nv.Value, err = (helper.NewConverter()).ConvertValue(nv.Value)
	return
}

// Exec 实现driver.Stmt接口
// Deprecated: 废弃,实现 driver.StmtExecContext 接口代替
func (stmt *mysqlStmt) Exec(args []driver.Value) (driver.Result, error) {
	if stmt.mc.ct.Closed() {
		stmt.mc.GetConfig().Logger.Print(ErrInvalidConn)
		return nil, driver.ErrBadConn
	}
	return stmt.exec(args)
}

// Query 实现driver.Stmt接口
func (stmt *mysqlStmt) Query(args []driver.Value) (driver.Rows, error) {
	return stmt.query(args)
}

func (stmt *mysqlStmt) exec(args []driver.Value) (driver.Result, error) {
	// Send command
	err := stmt.writeExecutePacket(args)
	if err != nil {
		return nil, stmt.mc.markBadConn(err)
	}

	mc := stmt.mc
	stmt.mc.clearResult()

	// Read Result
	var ay any
	var rData []byte
	var resLen int
	if ay, err = mc.GetReader()(mp.NotUntilEOF); nil != err {
		return nil, err
	}

	rData = ay.([]byte)

	if resLen, err = mc.ct.ReadResultSetHeaderPacket(mc.GetResult(), rData, mc.GetReader()); nil != err {
		return nil, err
	}

	if resLen > 0 {
		// Columns
		if _, err = mc.ct.Receive(mp.UntilEOF, mc.closer); err != nil {
			return nil, err
		}

		// Rows
		if _, err := mc.ct.Receive(mp.UntilEOF, mc.closer); err != nil {
			return nil, err
		}
	}

	var sts any

	for mc.status&mp.StatusMoreResultsExists != 0 {
		if ay, err = mc.GetReader()(mp.NotUntilEOF); nil != err {
			return nil, err
		}

		rData = ay.([]byte)

		if resLen, err = mc.ct.ReadResultSetHeaderPacket(mc.GetResult(), rData, mc.GetReader()); nil != err {
			return nil, err
		}
		if resLen > 0 {
			// columns
			if sts, err = mc.ct.Receive(mp.UntilEOF, mc.closer); err != nil {
				return nil, err
			}
			mc.status = sts.(mp.StatusFlag)
			// rows
			if sts, err = mc.ct.Receive(mp.UntilEOF, mc.closer); err != nil {
				return nil, err
			}
			mc.status = sts.(mp.StatusFlag)
		}
	}

	copied := mc.result
	return &copied, nil
}

func (stmt *mysqlStmt) query(args []driver.Value) (*BinaryRows, error) {
	if stmt.mc.ct.Closed() {
		stmt.mc.GetConfig().Logger.Print(ErrInvalidConn)
		return nil, driver.ErrBadConn
	}
	// Send command
	err := stmt.writeExecutePacket(args)
	if err != nil {
		return nil, stmt.mc.markBadConn(err)
	}

	mc := stmt.mc

	// Read Result
	stmt.mc.clearResult()
	var ay any
	var rData []byte
	var resLen int
	if ay, err = mc.GetReader()(mp.NotUntilEOF); nil != err {
		return nil, err
	}

	rData = ay.([]byte)

	if resLen, err = mc.ct.ReadResultSetHeaderPacket(mc.GetResult(), rData, mc.GetReader()); nil != err {
		return nil, err
	}

	brs := new(BinaryRows)

	if resLen > 0 {
		brs.mc = mc
		err = brs.readColumns(resLen)

	} else {
		brs.rs.Done()

		switch err := brs.NextResultSet(); err {
		case nil, io.EOF:
			return brs, nil
		default:
			return nil, err
		}
	}

	return brs, err
}

// Prepare Result Packets
// http://dev.mysql.com/doc/internals/en/com-stmt-prepare-response.html
func (stmt *mysqlStmt) readPrepareResultPacket(data []byte, flags mp.CapabilityFlag, rejectReadOnly bool) (uint16, error) {
	spp := new(parser.StPrepareParser)
	x, err := spp.Parse(data, flags, rejectReadOnly)
	stmt.id = x.StmtId
	stmt.paramCount = int(x.ParamNum)
	return x.ColumnNum, err
}

// Execute Prepared Statement
// http://dev.mysql.com/doc/internals/en/com-stmt-execute.html
func (stmt *mysqlStmt) writeExecutePacket(args []driver.Value) error {
	if len(args) != stmt.paramCount {
		return fmt.Errorf(
			"argument count mismatch (got: %d; has: %d)",
			len(args),
			stmt.paramCount,
		)
	}

	return stmt.mc.GetClient().SendStmtExec(stmt.id, stmt.paramCount, args)
}
