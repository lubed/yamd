package conn

import (
	"database/sql/driver"
	"encoding/binary"
	"fmt"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/bdt"
	"gitee.com/lubed/mysql-protocol/helper"
	"gitee.com/lubed/mysql-protocol/parser"
	"io"
	"math"
)

type BinaryRows struct {
	mysqlRows
}

func (brs *BinaryRows) NextResultSet() error {
	resLen, err := brs.nextNotEmptyResultSet()
	if err != nil {
		return err
	}

	err = brs.readColumns(resLen)
	return err
}

func (brs *BinaryRows) Next(dest []driver.Value) error {
	if mc := brs.mc; mc != nil {
		if err := mc.ct.Error(); err != nil {
			return err
		}

		// Fetch next row from stream
		return brs.readRow(dest)
	}
	return io.EOF
}

// http://dev.mysql.com/doc/internals/en/binary-protocol-resultset-row.html
func (brs *BinaryRows) readRow(dest []driver.Value) error {
	var ay any
	var err error
	mc := brs.mc
	lei := bdt.LenEncodedInt()
	ay, err = mc.ct.Receive(mp.NotUntilEOF, mc.closer)
	if err != nil {
		return err
	}
	data := ay.([]byte)
	// packet indicator [1 byte]
	if data[0] != mp.OK {
		// EOF Packet
		if data[0] == mp.EOF && len(data) == 5 {
			brs.mc.status = mp.ReadStatus(data[3:])
			brs.rs.Done()
			if !brs.HasNextResultSet() {
				brs.mc = nil
			}
			return io.EOF
		}
		mc := brs.mc
		brs.mc = nil
		mpr := parser.NewMyParser()
		// Error otherwise
		return mpr.ParseErrorPacket(data, mc.GetConfig().RejectReadOnly)
	}

	// NULL-bitmap,  [(column-count + 7 + 2) / 8 bytes]
	pos := 1 + (len(dest)+7+2)>>3
	nullMask := data[1:pos]

	for i := range dest {
		// Field is NULL
		// (byte >> bit-pos) % 2 == 1
		if ((nullMask[(i+2)>>3] >> uint((i+2)&7)) & 1) == 1 {
			dest[i] = nil
			continue
		}
		field, err := brs.rs.GetColumnByIndex(i)
		// Convert to byte-coded string
		switch field.FieldType {
		case mp.FieldTypeNULL:
			dest[i] = nil
			continue

		// Numeric Types
		case mp.FieldTypeTiny:
			if field.Flags&mp.FlagUnsigned != 0 {
				dest[i] = int64(data[pos])
			} else {
				dest[i] = int64(int8(data[pos]))
			}
			pos++
			continue

		case mp.FieldTypeShort, mp.FieldTypeYear:
			if field.Flags&mp.FlagUnsigned != 0 {
				dest[i] = int64(binary.LittleEndian.Uint16(data[pos : pos+2]))
			} else {
				dest[i] = int64(int16(binary.LittleEndian.Uint16(data[pos : pos+2])))
			}
			pos += 2
			continue

		case mp.FieldTypeInt24, mp.FieldTypeLong:
			if field.Flags&mp.FlagUnsigned != 0 {
				dest[i] = int64(binary.LittleEndian.Uint32(data[pos : pos+4]))
			} else {
				dest[i] = int64(int32(binary.LittleEndian.Uint32(data[pos : pos+4])))
			}
			pos += 4
			continue

		case mp.FieldTypeLongLong:
			if field.Flags&mp.FlagUnsigned != 0 {
				val := binary.LittleEndian.Uint64(data[pos : pos+8])
				if val > math.MaxInt64 {
					dest[i] = helper.Uint64ToString(val)
				} else {
					dest[i] = int64(val)
				}
			} else {
				dest[i] = int64(binary.LittleEndian.Uint64(data[pos : pos+8]))
			}
			pos += 8
			continue

		case mp.FieldTypeFloat:
			dest[i] = math.Float32frombits(binary.LittleEndian.Uint32(data[pos : pos+4]))
			pos += 4
			continue

		case mp.FieldTypeDouble:
			dest[i] = math.Float64frombits(binary.LittleEndian.Uint64(data[pos : pos+8]))
			pos += 8
			continue

		// Length coded Binary Strings
		case mp.FieldTypeDecimal, mp.FieldTypeNewDecimal, mp.FieldTypeVarChar,
			mp.FieldTypeBit, mp.FieldTypeEnum, mp.FieldTypeSet, mp.FieldTypeTinyBLOB,
			mp.FieldTypeMediumBLOB, mp.FieldTypeLongBLOB, mp.FieldTypeBLOB,
			mp.FieldTypeVarString, mp.FieldTypeString, mp.FieldTypeGeometry, mp.FieldTypeJSON:
			var isNull bool
			var n int
			dest[i], isNull, n, err = lei.DecodeStr(data[pos:])
			pos += n
			if err == nil {
				if !isNull {
					continue
				} else {
					dest[i] = nil
					continue
				}
			}
			return err

		case
			mp.FieldTypeDate, mp.FieldTypeNewDate, // Date YYYY-MM-DD
			mp.FieldTypeTime,                            // Time [-][H]HH:MM:SS[.fractal]
			mp.FieldTypeTimestamp, mp.FieldTypeDateTime: // Timestamp YYYY-MM-DD HH:MM:SS[.fractal]

			num, isNull, n := lei.Decode(data[pos:])
			pos += n

			switch {
			case isNull:
				dest[i] = nil
				continue
			case field.FieldType == mp.FieldTypeTime:
				// database/sql does not support an equivalent to TIME, return a string
				var dstlen uint8
				switch decimals := field.Decimals; decimals {
				case 0x00, 0x1f:
					dstlen = 8
				case 1, 2, 3, 4, 5, 6:
					dstlen = 8 + 1 + decimals
				default:
					return fmt.Errorf(
						"protocol error, illegal decimals value %d",
						field.Decimals,
					)
				}
				dest[i], err = helper.FormatBinaryTime(data[pos:pos+int(num)], dstlen)
			case brs.mc.GetConfig().ParseTime:
				dest[i], err = helper.ParseBinaryDateTime(num, data[pos:], brs.mc.GetConfig().Loc)
			default:
				var dstlen uint8
				if field.FieldType == mp.FieldTypeDate {
					dstlen = 10
				} else {
					switch decimals := field.Decimals; decimals {
					case 0x00, 0x1f:
						dstlen = 19
					case 1, 2, 3, 4, 5, 6:
						dstlen = 19 + 1 + decimals
					default:
						return fmt.Errorf(
							"protocol error, illegal decimals value %d",
							field.Decimals,
						)
					}
				}
				dest[i], err = helper.FormatBinaryDateTime(data[pos:pos+int(num)], dstlen)
			}

			if err == nil {
				pos += int(num)
				continue
			} else {
				return err
			}

		// Please report if this happens!
		default:
			return fmt.Errorf("unknown field type %d", field.FieldType)
		}
	}

	return nil
}
