package yamd

import (
	"context"
	"database/sql/driver"
	"gitee.com/lubed/buf/dblbuf"
	clt "gitee.com/lubed/mysql-client"
	"gitee.com/lubed/mysql-client/auth"
	"gitee.com/lubed/mysql-client/config"
	mp "gitee.com/lubed/mysql-protocol"
	"gitee.com/lubed/mysql-protocol/dsn"
	"gitee.com/lubed/yamd/conn"
)

type Connector struct {
	cfg     *dsn.Config // immutable private copy.
	isDebug bool
}

func NewConnector(cfg *dsn.Config) *Connector {
	return &Connector{
		cfg:     cfg,
		isDebug: isDebug,
	}
}

// Connect 实现 driver.Connector 接口，返回一个到MySQL数据库的连接
func (c *Connector) Connect(ctx context.Context) (driver.Conn, error) {
	var err error
	var mc *conn.MysqlConn
	//SetDebugMode(DebugOff) //设置调试模式
	ct := c.initMyClient()
	mc = conn.NewMyConn(c.cfg, isDebug)

	if err = mc.Init(ctx, ct); nil != err {
		return nil, err
	}

	// 认证
	acg := ct.GetConfig().ToACG()
	cltAth := auth.NewCltAuth(acg, ct)
	//ath := authentication.NewAuth(mc)

	if _, err = cltAth.Auth(); nil != err {
		return nil, err
	}

	// 发送初始设置相关命令
	initRequest := conn.NewMyInitRequest(mc)

	// MySQL认证通过后 根据DSN中设置的参数 向服务器发送相关设置命令
	if err = initRequest.DoInitialize(); nil != err {
		mc.Close()
		return nil, err
	}

	return mc, nil
}

// Driver 实现 driver.Connector 接口,返回 Yamd
func (c *Connector) Driver() driver.Driver {
	return &Yamd{}
}

// initMyClient 初始化MySQL客户端
func (c *Connector) initMyClient() *clt.MyClient {
	bCfg := dblbuf.NewCf(mp.DefaultPacketSize, mp.MaxCachedBufSize, mp.MaxPacketSize)
	buf := *dblbuf.NewBf(bCfg)
	cfg := c.GetCltConfig()
	ct := clt.NewMyClient(cfg, buf)
	ct.InitHandler(mp.MaxPacketSize, isDebug)
	return ct
}

func (c *Connector) GetCltConfig() *config.Config {
	cfg := config.NewCltConfig()
	cfg.Net = c.cfg.Net
	cfg.Addr = c.cfg.Addr
	cfg.ReadTimeout = c.cfg.ReadTimeout
	cfg.WriteTimeout = c.cfg.WriteTimeout
	cfg.CheckConnLiveness = c.cfg.CheckConnLiveness
	cfg.AllowAllFiles = c.cfg.AllowAllFiles
	cfg.User = c.cfg.User
	cfg.Passwd = c.cfg.Passwd
	cfg.CollationId, _ = mp.CollationNameToId(c.cfg.Collation)
	cfg.DBName = c.cfg.DBName
	cfg.PubKey = c.cfg.PubKey                                     // Server public key
	cfg.TLS = c.cfg.TLS                                           // TLS configuration, its priority is higher than TLSConfig
	cfg.AllowCleartextPasswords = c.cfg.AllowCleartextPasswords   // Allows the cleartext client side plugin
	cfg.AllowFallbackToPlaintext = c.cfg.AllowFallbackToPlaintext // Allows fallback to unencrypted connection if server does not support TLS
	cfg.AllowNativePasswords = c.cfg.AllowNativePasswords         // Allows the native password authentication method
	cfg.AllowOldPasswords = c.cfg.AllowOldPasswords               // Allows the old insecure password method
	cfg.ClientFoundRows = c.cfg.ClientFoundRows                   // Return number of matching rows instead of rows changed
	cfg.MultiStatements = c.cfg.MultiStatements                   // Allow multiple statements in one query
	cfg.RejectReadOnly = c.cfg.RejectReadOnly
	cfg.Loc = c.cfg.Loc                   // Location for time.Time values //time.UTC
	cfg.TimeTruncate = c.cfg.TimeTruncate // Truncate time.Time values to the specified duration
	return cfg
}
