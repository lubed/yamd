
## 支持

* Go 1.21
* MySQL (5.7+) , MariaDB (10.3+).

---------------------------------------

## 安装


```bash
go get -u gitee.com/yamd
```

## 使用

```go
import (
"database/sql"
"time"

_ "gitee.com/yamd"
)

// ...
var db *sql.DB
var err error

if db, err = sql.Open("mysql", "user:password@/dbname"); nil != err {
    panic(err)
}

//参数设置
db.SetConnMaxLifetime(time.Minute * 3)
db.SetMaxOpenConns(10)
db.SetMaxIdleConns(10)
// ...
```

