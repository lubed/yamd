package excep

import (
	"errors"
)

// Various errors the driver might return. Can change between driver versions.
var (
	ErrInvalidConn = errors.New("invalid connection")
	ErrMalformPkt  = errors.New("malformed packet")
	ErrNoTLS       = errors.New("TLS requested but server does not support TLS")
	ErrOldProtocol = errors.New("MySQL server does not support required protocol 41+")
)
