package excep

const (
	None                 = 0
	PktTooLarge          = 100001
	BufGetFailed         = 100002
	PktOptionSqGetFailed = 100003
)
