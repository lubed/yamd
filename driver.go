package yamd

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"gitee.com/lubed/mysql-protocol/dsn"
)

type Yamd struct{}

// Open new Connection.
// See https://gitee.com/lubed/mysql#dsn-data-source-name for how
// the DSN string is formatted
func (y Yamd) Open(s string) (driver.Conn, error) {
	var cfg *dsn.Config
	var err error

	if cfg, err = dsn.ParseDSN(s); err != nil {
		return nil, err
	}

	c, err := newConnector(cfg)
	return c.Connect(context.Background())
}

// This variable can be replaced with -ldflags like below:
// go build "-ldflags=-X gitee.com/lubed/mysql.driverName=custom"
const (
	DebugOn  = true
	DebugOff = false
)

var (
	driverName      = "mysql"
	isDebug    bool = DebugOff
)

func init() {
	if driverName != "" {
		sql.Register(driverName, &Yamd{})
	}
}

func GetDriverName() string {
	return driverName
}

func SetDebugMode(isEnable bool) {
	isDebug = isEnable
}

// NewConnector returns new driver.Connector.
func newConnector(cfg *dsn.Config) (driver.Connector, error) {
	cfg = cfg.Clone()
	// normalize the contents of cfg so calls to NewConnector have the same
	// behavior as Yamd.OpenConnector
	if err := cfg.Normalize(); err != nil {
		return nil, err
	}
	return NewConnector(cfg), nil
}

// OpenConnector implements driver.DriverContext.
func (y Yamd) OpenConnector(s string) (driver.Connector, error) {
	cfg, err := dsn.ParseDSN(s)
	if err != nil {
		return nil, err
	}
	return newConnector(cfg)
}
