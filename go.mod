module gitee.com/lubed/yamd

go 1.21

require (
	gitee.com/lubed/buf v0.3.0
	gitee.com/lubed/client v0.3.1
	gitee.com/lubed/logger v0.1.0
	gitee.com/lubed/mysql-client v0.1.3
	gitee.com/lubed/mysql-protocol v0.2.8
)
